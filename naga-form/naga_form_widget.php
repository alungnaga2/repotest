<?php

add_action('widgets_init', create_function('', 'return register_widget("naga_form_widget");'));

class naga_form_widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(false, $name = 'Naga Form');
	}

	function widget($args, $instance)
	{
		extract($args);
		
		echo NagaForm()->callback_naga_form();
			
		
	}

	function form($instance) 
	{

	}

	function update($new_instance, $old_instance)
	{

	}
}