<?php
/**
 * Plugin Name: Naga Form
 * Description: Naga Form for SS WP Assessment
 * Version: 1.0.0
 * Author: Ganda G. S. Sinaga
 */

defined('NAGA_PLUGIN_URL') || define('NAGA_PLUGIN_URL', plugins_url('/', __FILE__));
defined('NAGA_PLUGIN_FILE') || define('NAGA_PLUGIN_FILE', __FILE__);

if (!class_exists('NagaForm')) {

	class NagaForm
	{
		function __construct()
		{
			register_activation_hook(NAGA_PLUGIN_FILE, array($this, 'install'));

			// ajax for frontend when user add comment on the form
			add_action('wp_ajax_naga_form_save', array($this, 'callback_ajax_naga_form_save'));
			add_action('wp_ajax_nopriv_naga_form_save', array($this, 'callback_ajax_naga_form_save') );

			add_action('wp_enqueue_scripts', array($this, 'public_enqueue_scripts'));

			// add shortcode for frontend
			add_shortcode('naga_form', array($this, 'callback_naga_form'));
			add_shortcode('naga_form_data', array($this, 'callback_naga_form_data'));

			add_action('admin_menu', array($this, 'add_admin_menu'));

			require dirname(NAGA_PLUGIN_FILE)."/naga_form_widget.php";
		}

		function add_admin_menu()
		{
			add_menu_page( "Naga Form", "Naga Form", "manage_options", "naga-form", array($this, "admin_naga_form") );
			add_submenu_page('naga-form', __('Settings'), __('Settings'), 'manage_options', 'naga-form-setup', array($this, 'admin_naga_form_setup'));

		}

		function admin_naga_form()
		{
			global $wpdb;

			// get all comment from database and showed to the table
			$k_msg = "select * from ".$wpdb->prefix."naga_form";
			$q_msg = $wpdb->get_results($k_msg);
			?>

			<div class="wrap">
            	<h1 class="wp-heading-inline"><?php echo get_admin_page_title(); ?></h1>
            	<hr class="wp-header-end">

            	<table class="wp-list-table widefat fixed striped">
                    <thead>
                        <tr>
                            <td class="manage-column column-cb check-column">
                                <input type="checkbox" name="check_all" id="check_all">
                            </td>
                            <th class="manage-column">Name</th>
                            <th class="manage-column">Email</th>
                            <th class="manage-column">Message</th>
                            <th class="manage-column">Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php foreach ($q_msg as $msg) : ?>
                    		<tr>
	                        	<td><input type="checkbox" name="naga_data[]" value="<?php echo $msg->id ?>"></td>
	                        	<td><?php echo $msg->name ?></td>
	                        	<td><?php echo $msg->email ?></td>
	                        	<td><?php echo $msg->message ?></td>
	                        	<td>
	                        		<?php if (!$msg->confirm) : ?>
	                        			<a href="javascript:void(0)">Confirm</a>
	                        			/
	                        		<?php endif; ?>
	                        		<a href="">Delete</a>
	                        	</td>
	                        </tr>
                    	<?php endforeach; ?>
	                        
                    </tbody>
                        
                </table>

                <a href="" class="button">Delete</a>
            </div>

			<?php
		}

		function admin_naga_form_setup()
		{
			// page for setup the receipient email
			// the receipient will receive the newest email send by cron
			?>

			<div class="wrap">
            	<h1 class="wp-heading-inline">Naga Form <?php echo get_admin_page_title(); ?></h1>
            	<hr class="wp-header-end">

            	<div>
            		<label>Email Receipient<br><input type="text" name=""></label>
            	</div>
            	<a href="" class="button">Save</a>
            </div>
			<?php
		}

		function callback_naga_form($attr = null)
		{
			// form for frontend call by shortcode
			ob_start();
	        ?>
	        <form name="naga-form" id="naga-form">
	        	<div>
	        		<label>
	        			Name<br>
	        			<input type="text" name="naga_name">
	        		</label>
	        	</div>
	        	<div>
	        		<label>
	        			Email<br>
	        			<input type="email" name="naga_email">
	        		</label>
	        	</div>
	        	<div>
	        		<label>
	        			Message<br>
	        			<textarea name="naga_message"></textarea>
	        		</label>
	        	</div>
	        	<div>
	        		<input type="submit" value="Save">
	        	</div>
	        	<input type="hidden" name="action" value="naga_form_save">
	        </form>
	        <?php
	        return ob_get_clean();
		}

		function callback_naga_form_data($attr)
		{
			// showing the user comment to the frontend call by shortcode
			global $wpdb;

			$k_msg = "select * from ".$wpdb->prefix."naga_form where confirm = 1";
			$q_msg = $wpdb->get_results($k_msg);

			ob_start();

			?>

			<div>
				<?php foreach ($q_msg as $msg) : ?>
					<div>Name: <?php echo $msg->name ?></div>
					<div>Email: <?php echo $msg->email ?></div>
					<div style="border-bottom: solid 1px;"><?php echo $msg->message ?></div>
				<?php endforeach; ?>
			</div>

			<?php

			return ob_get_clean();
		}

		function callback_ajax_naga_form_save()
		{
			if (isset($_POST["naga_name"]))
			{
				// save the user comment to the database triggered from frontend
				global $wpdb;

				$response = [];
				$response["status"] = true;

				$naga_name = $_POST["naga_name"];
				$naga_email = $_POST["naga_email"];
				$naga_message = $_POST["naga_message"];

				$response["status"] = $wpdb->insert($wpdb->prefix."naga_form", array("name" => $naga_name,"email" => $naga_email,"message" => $naga_message));

				wp_send_json($response);
			}
		}

		function public_enqueue_scripts()
		{
			wp_enqueue_script('jquery');
			wp_localize_script( "jquery", "naga_form", array('ajaxurl' => admin_url( 'admin-ajax.php' )));
			wp_enqueue_script('pp-public-proposal', NAGA_PLUGIN_URL . 'js/naga_form.js', ['jquery'], null, true);
		}

		function install()
		{
			// create tables of database
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

			$sqldb = array();

	        $sqldb[] = "CREATE TABLE ".$wpdb->prefix."naga_form (
	                      id bigint(20) NOT NULL AUTO_INCREMENT,
	                      name varchar(100) NOT NULL,
	                      email varchar(100) NOT NULL,
	                      message TEXT NOT NULL,
	                      confirm tinyint(10) NOT NULL,
	                      PRIMARY KEY  (id)
	                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;";

	        foreach ($sqldb as $tabledb)
	        {
	            dbDelta( $tabledb );
	        }
		}
	}
}

if (!function_exists('NagaForm')) {

    function NagaForm() {
    	$new = new NagaForm;
        return $new;
    }

    NagaForm();
}