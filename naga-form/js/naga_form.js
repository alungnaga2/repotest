(function (e) {
    e(window.jQuery, window, document);
})(function ($) {
    $(function () {
        $("body").on("submit", "#naga-form", function(){
            $.post(naga_form.ajaxurl, $(this).serialize(), function (data) {
                if (data.status)
                {
                    $("#naga-form").find("input[name='naga_name']").val("");
                    $("#naga-form").find("input[name='naga_email']").val("");
                    $("#naga-form").find("textarea[name='naga_message']").val("");
                }
            });

            return false;
        });
    });
});