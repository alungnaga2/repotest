<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<table border="1" width="100%">
		<tr>
			<td rowspan="2" valign="top" width="200"><?php get_sidebar(); ?></td>
			<td>
				<?php
				// call the Naga Menu
        		wp_nav_menu( array(
				    'menu'   => 'Naga Menu',
				    'container' => ''
				) );
        	?>
			</td>
		</tr>