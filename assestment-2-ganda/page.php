<?php get_header(); ?>

<tr>
	<td height="500" valign="top">
		<?php
      	if (have_posts()) {
		    while (have_posts()) {
		        the_post();
		        // the_title();
		        
		        the_content();
		    }
		}
      ?>
	</td>
</tr>

<?php get_footer(); ?>
