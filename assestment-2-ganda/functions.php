<?php

// add pages and menus when activate  the theme
add_action("after_switch_theme", "mytheme_do_something");

function mytheme_do_something()
{
	$arr_pages[] = array("post_title" => "Home", "post_name" => "home", "post_content" => "Home", "option_name" => "pp_dashboard_page_id");
    $arr_pages[] = array("post_title" => "Sample Page 1", "post_name" => "sample-page-1", "post_content" => "Sample Page 1", "option_name" => "pp_proposal_page_id");
    $arr_pages[] = array("post_title" => "Sample Page 2", "post_name" => "sample-page-2", "post_content" => "Sample Page 2", "option_name" => "pp_template_page_id");

	$menu_name = 'Naga Menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );
	if( !$menu_exists)
	    $menu_id = wp_create_nav_menu($menu_name);
	else
	    $menu_id = $menu_exists->term_id;

	foreach ($arr_pages as $page)
	{
		$arr_setups = [];
        $arr_setups = array('post_title' => $page["post_title"],
                            'post_type' => 'page',
                            'post_content' => $page["post_content"],
                            'post_status' => 'publish',
                            'post_name' => $page["post_name"],
                            'comment_status' => 'closed',
                            'ping_status' => 'closed'
                        );

        // get if the post_name exist
        $post_found = array("name" => $page["post_name"], "post_type" => "page");
        $obj_found = get_posts($post_found);

        if ($obj_found)
        {
            $page_id = $obj_found[0]->ID;
            $arr_setups["ID"] = $page_id;
            wp_update_post( $arr_setups );

            $menu_item_db_id = get_post_meta( $page_id, "menu_item_db_id", true );
        }
        else
        {
            $page_id = wp_insert_post($arr_setups);
            $menu_item_db_id = 0;
        }

        // set the menu
        $db_id = wp_update_nav_menu_item($menu_id, $menu_item_db_id, array(
                                                                        'menu-item-title' =>  __($page["post_title"]),
                                                                        'menu-item-classes' => "css-".$page["post_name"],
                                                                        'menu-item-object' => 'page',
                                                                        'menu-item-object-id' => $page_id,
                                                                        'menu-item-type' => 'post_type',
                                                                        'menu-item-status' => 'publish'));

        update_post_meta( $page_id, "menu_item_db_id", $db_id );
	}
}

// add javascript
add_action('wp_enqueue_scripts', 'public_enqueue_scripts', 11);

function public_enqueue_scripts()
{
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'app-js', get_template_directory_uri() . '/js/app.js', [], null, true);
}