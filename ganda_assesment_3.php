<?php
	/*
		please require/include this file in your current wp theme
	*/

	// set the filter to change the timezone
	add_filter( "assesment_3_timezones", "assesment_3_timezones" );
	function assesment_3_timezones($params)
	{
		// get all the list of timezone
		$tmp_list = [];
		$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		foreach ($tzlist as $list)
		{
			$tmp_list[$list] = $list;
		}
		// change the array of timezone
		$params = $tmp_list;
		return $params;
	}

	// set the text above the dropdown of timezone
	add_action( "assesment_3_after_render", "assesment_3_after_render" );
	function assesment_3_after_render()
	{
		echo "Silahkan pilih timezone Anda:<br>";
	}
?>